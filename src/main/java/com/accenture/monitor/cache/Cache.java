package com.accenture.monitor.cache;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class Cache {

	private static Cache cache;
	private Properties properties = new Properties();
	List<File> listFilesToMove = new LinkedList<File>();
	List<File> listTotalFilesMoved = new LinkedList<File>();

	private Cache() {
		// Constructor privado- Singleton
	}

	public static Cache getCache() {
		if (cache == null) {
			cache = new Cache();
		}
		return cache;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public List<File> getListFilesToMove() {
		return listFilesToMove;
	}

	public void setListFilesToMove(List<File> listFilesToMove) {
		this.listFilesToMove = listFilesToMove;
	}

	public List<File> getListTotalFilesMoved() {
		return listTotalFilesMoved;
	}

	public void setListTotalFilesMoved(List<File> listTotalFiles) {
		this.listTotalFilesMoved = listTotalFiles;
	}

}
