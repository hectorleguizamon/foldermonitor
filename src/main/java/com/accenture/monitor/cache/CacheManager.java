/**
 * 
 */
package com.accenture.monitor.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

public class CacheManager {

	private static final String ROUTE_CONFIGURATION = "config/monitor.properties";
	private static final String NAME_DIRECTORY = "rute.directory1";
	private static final Logger logger = Logger.getLogger(CacheManager.class);

	public void readProperties() {
		logger.trace("Lee las propiedades de configuracion");
		InputStream archivo;
		Cache cache = Cache.getCache();
		Properties properties = new Properties();

		try {
			archivo = new FileInputStream(ROUTE_CONFIGURATION);
			properties.load(archivo);
			archivo.close();
		} catch (FileNotFoundException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		cache.setProperties(properties);
	}

	public void readFiles() {
		logger.trace("Lee los archivos que hay en el directorio");
		Cache cache = Cache.getCache();
		Properties properties = cache.getProperties();
		String ruteDirectory1 = properties.getProperty(NAME_DIRECTORY);
		File directory1 = new File(ruteDirectory1);
		File[] listFiles = directory1.listFiles();

		List<File> listFilesToMove = Arrays.asList(listFiles);
		cache.setListFilesToMove(listFilesToMove);
	}

}
