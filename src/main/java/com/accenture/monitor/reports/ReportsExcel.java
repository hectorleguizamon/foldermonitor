package com.accenture.monitor.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.record.cf.PatternFormatting;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.accenture.monitor.cache.Cache;
import com.accenture.monitor.utils.DateUtils;

public class ReportsExcel {

	private static final Logger logger = Logger.getLogger(ReportsExcel.class);
	private static final String NAME_EXCEL = "name.excel";
	private static final String NAME_PAGE_EXCEL = "name.page.excel";
	private static final String NAME_FIRST_TITLE_EXCEL = "name.first.title.excel";
	private Properties properties;
	private Cache cache;
	private File archivo;
	private Workbook workbook;
	private Sheet pagina;
	private Row fila;

	public void generateExcelFiles() {

		cache = Cache.getCache();
		ReportsExcel reportsExcel = new ReportsExcel();

		reportsExcel.setNameFile();
		reportsExcel.makeWorkbook();
		reportsExcel.makeFirstRow();
		reportsExcel.makeRows();
		reportsExcel.writeFile();

	}

	private void setNameFile() {
		logger.trace("Establece ruta y nombre del archivo Excel");
		cache = Cache.getCache();
		properties = cache.getProperties();
		String nameExcel = properties.getProperty(NAME_EXCEL);
		String date = DateUtils.getCurrentDateAndHour();
		String routeFileExcel = MessageFormat.format("reports/{0}{1}.xlsx", nameExcel, date);
		archivo = new File(routeFileExcel);
	}

	private void makeWorkbook() {
		logger.trace("Realiza espacio de trabajo de Excel");
		workbook = new XSSFWorkbook();
		String namePage = properties.getProperty(NAME_PAGE_EXCEL);
		pagina = workbook.createSheet(namePage);
	}

	private void makeFirstRow() {
		logger.trace("Configura la primera fila de la tabla de Excel");
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		style.setFillPattern(PatternFormatting.SOLID_FOREGROUND);
		String firstTitle = properties.getProperty(NAME_FIRST_TITLE_EXCEL);
		fila = pagina.createRow(0);

		Cell celda = fila.createCell(0);
		celda.setCellStyle(style);
		celda.setCellValue(firstTitle);
	}

	private void makeRows() {
		logger.trace("Inserta en las filas los nombres de los archivos movidos");
		List<File> listTotalFiles = cache.getListTotalFilesMoved();
		Iterator<File> it = listTotalFiles.iterator();
		File file;
		int cont = 1;

		while (it.hasNext()) {
			file = it.next();
			fila = pagina.createRow(cont);
			cont++;
			Cell celda = fila.createCell(0);
			celda.setCellValue(file.getName());
		}
	}

	private void writeFile() {
		logger.trace("Crear el archivo de Excel");
		try {
			FileOutputStream salida = new FileOutputStream(archivo);
			workbook.write(salida);
			logger.info("Archivo creado existosamente");
		} catch (FileNotFoundException ex) {
			logger.error(ex);
			logger.error("Archivo no localizable en sistema de archivos");
		} catch (IOException ex) {
			logger.error(ex);
			logger.error("Error de entrada/salida");
		}
		List<File> listTotalFiles = cache.getListTotalFilesMoved();
		Integer numberFiles = listTotalFiles.size();
		logger.info("Numero de archivos registrados: " + numberFiles);
	}

}
