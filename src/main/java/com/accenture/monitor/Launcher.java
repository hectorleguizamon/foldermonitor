/**
 * @author hector.leguizamon
 *
 */
package com.accenture.monitor;

import java.text.MessageFormat;
import org.apache.log4j.Logger;
import com.accenture.monitor.cache.CacheManager;
import com.accenture.monitor.files.FilesMonitor;
import com.accenture.monitor.reports.ReportsExcel;

public class Launcher {
	public static final Logger logger = Logger.getLogger(Launcher.class);
	static long baseTime = System.currentTimeMillis();

	public static void main(String[] args) {

		Threads threadLauncher = new Threads();
		logger.info("Empieza el monitor de archivos");
		threadLauncher.start();

	}

	public void readCache() {
		logger.trace("Lectura de cache");
		CacheManager cacheManager = new CacheManager();
		cacheManager.readProperties();
		cacheManager.readFiles();
	}

	public void doMoveFiles() {
		logger.trace("Mueve archivos entre las 2 carpetas");
		FilesMonitor filesMonitor = new FilesMonitor();
		filesMonitor.moveFiles();
	}

	public void doGenerateReports() {
		logger.trace("Genera reporte");
		ReportsExcel generateReportsExcel = new ReportsExcel();
		generateReportsExcel.generateExcelFiles();
	}

	static void showElapsedTime(String message) {
		long elapsedTime = System.currentTimeMillis() - baseTime;
		String messageElapsedTime;
		messageElapsedTime = MessageFormat.format("{0} en {1} segundos", message, (elapsedTime / 1000.0));
		logger.trace(messageElapsedTime);
	}

	static long getTime() {
		long elapsedTime = System.currentTimeMillis() - baseTime;
		return elapsedTime / 1000;
	}
}

class Threads {
	int data = -1;

	Threads() { // constructor
		Launcher.showElapsedTime("Hilo creado");
	}

	public void run() {
		Launcher.showElapsedTime("Hilo Iniciado");
		Launcher launcher = new Launcher();
		while (Launcher.getTime() < 120.0) {
			launcher.readCache();
			launcher.doMoveFiles();
		}
		data = 999;
		launcher.doGenerateReports();
		Launcher.showElapsedTime("Hilo finalizado");
	}

	public void start() {
		run();
	}
}
