package com.accenture.monitor.utils;

import java.text.Format;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

public class DateUtils {
       
       static final Logger logger = Logger.getLogger(DateUtils.class);
       
       private DateUtils() {
    	   //constructor
       }

       public static String getCurrentDate() {
             Date date = new Date();
             SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
             return formatter.format(date);
       }

       public static String getCurrentDateAndHour() {
             Date date = new Date();
             SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmm");
             return formatter.format(date);
       }

       public static Date getStringAsDate(String sDate, String pattern) throws ParseException {
             SimpleDateFormat formatter = new SimpleDateFormat(pattern);
             return formatter.parse(sDate);
       }

       public static String getDateAsString(Date date, String pattern) {
             Format formatter = new SimpleDateFormat(pattern);
             return formatter.format(date);
       }

       public static boolean isAfter(Date firstDate, Date secondDate) {
             return firstDate.compareTo(secondDate) >= 0;
       }

       public static boolean isBefore(Date firstDate, Date secondDate) {
             return firstDate.compareTo(secondDate) <= 0;
       }

       public static boolean validateDate(Date startDate, Date endDate, Date date) {
             boolean isAfter = DateUtils.isAfter(endDate, date);
             boolean isBefore = DateUtils.isBefore(startDate, date);
             boolean isValid = false;
             if (isAfter && isBefore) {
                    isValid = true;
             }
             return isValid;
       }
       
       public static Date parsearDate(String name) {
             Date date = null;
             String fechaconCsv = name.substring(name.length() - 12, name.length());
             String dateString = fechaconCsv.substring(0, 8);
             try {
                    date = DateUtils.getStringAsDate(dateString, "yyyyMMdd");
             } catch (ParseException e) {
                    String msg = MessageFormat.format("Ocurrio un error parseando la fecha {0} del archivo {1}", dateString,
                                 name);
                    logger.error(msg);
             }
             return date;
       }

}
