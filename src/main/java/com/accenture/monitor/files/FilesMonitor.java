package com.accenture.monitor.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.accenture.monitor.cache.Cache;

public class FilesMonitor {
	private static final Logger logger = Logger.getLogger(FilesMonitor.class);
	private static final String RUTE_DIRECTORY_1 = "rute.directory1";
	private static final String RUTE_DIRECTORY_2 = "rute.directory2";
	private Cache cache;
	private List<File> listFilesToMove;
	private List<File> listTotalFiles;

	public void moveFiles() {
		cache = Cache.getCache();
		listFilesToMove = cache.getListFilesToMove();
		Properties properties = cache.getProperties();
		String ruteDirectory1 = properties.getProperty(RUTE_DIRECTORY_1);
		String ruteDirectory2 = properties.getProperty(RUTE_DIRECTORY_2);
		String originLocation;
		String destinationLocation;
		File file;

		Iterator<File> it = listFilesToMove.iterator();

		if (listFilesToMove.size() != 0) {
			FilesMonitor filesMonitor = new FilesMonitor();
			filesMonitor.addToListTotal();

			while (it.hasNext()) {
				file = it.next();
				try {
					String nameFile = file.getName();
					originLocation = MessageFormat.format("{0}/{1}", ruteDirectory1, nameFile);
					destinationLocation = MessageFormat.format("{0}/{1}", ruteDirectory2, nameFile);

					Path temp = Files.move(Paths.get(originLocation), Paths.get(destinationLocation));

					if (temp != null) {
						String messageSuccessfull;
						messageSuccessfull = MessageFormat.format("El archivo {0} fue movido correctamente", nameFile);
						logger.trace(messageSuccessfull);
					} else {
						String messageFailed;
						messageFailed = MessageFormat.format("El archivo {0} no fue movido", nameFile);
						logger.error(messageFailed);
					}
				} catch (IOException e) {
					logger.error(e);
				}

			}

		}

	}

	public void addToListTotal() {
		logger.trace("Actualiza lista total de archivos");
		Cache cache = Cache.getCache();
		listTotalFiles = cache.getListTotalFilesMoved();
		listFilesToMove = cache.getListFilesToMove();
		listTotalFiles.addAll(listFilesToMove);
	}
}
